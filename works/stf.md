---
layout : post
title : street fighter 2 illustrator
category : illustrator
date : 2015/12
thumbnail : https://farm2.staticflickr.com/1608/24493957443_72b53f8d0e_n.jpg
---

![street fighter 2 illustrator](https://farm2.staticflickr.com/1540/24553121164_fe14ef926b_o.jpg "street fighter 2 illustrator"){: .page_head_img}

## フラットなスト2
ガイルのフラットアイコンをなんとなく作ってみたら、うまくできた！だから、春麗とかエドモンド本田とかも作ってみました。スト2はコンプしたい。趣味です。

## illustratorで作成
トレースしつつ、ちょっと改変しつつ。ドット絵だけど、やっぱりかっこいいなと思いました。春麗のアイシャドウもかっこよくて、スト２の春麗とっても好きだな。エドモンド本田も、すごくかっこいいんですよね。プレイするときは、すごく下手でコマンド覚えられなくて、エドモンド本田ばっかり使ってたけど、こんなかっこよかったのかと再発見できましたw

## Adobe color CC
Adobe kulerという名前で昔はあったんだけど、強力パワーアップして、今はAdobe color CCというサービスになってます。
どんなサービスかというと、5つの色の配色リストが大量に置いてあるサイトです。
以前より、強力パワーアップと言ってたのは、illustrator CCとかCS6とかがあれば、illustrator上でこれらの配色の情報を見ることができるようになったことです。何がいいかというと、そのままイラレのスウォッチに取り入れることができるのです！

人気のテーマとか、普段はいろいろ見ていますが、今回は自分で作ってみることにしました。

[E-Honda](https://color.adobe.com/ja/cloud/aHR0cHM6Ly9jYy1hcGktYXNzZXRzLmFkb2JlLmlv/library/40B1AAA1-EAB3-47AC-9EBF-239B5E241A49/theme/a054a19f-1e4b-4017-a436-e3a468371a35/)

[Chun-Li](https://color.adobe.com/ja/cloud/aHR0cHM6Ly9jYy1hcGktYXNzZXRzLmFkb2JlLmlv/library/40B1AAA1-EAB3-47AC-9EBF-239B5E241A49/theme/c913816c-d69d-43ea-be8b-71231f5c81a2/)

E-Hondaは、うまくできた気がする。

### Adobe color CC

AdobeColorCCへのサイトリンク
[AdobeColorCC](https://color.adobe.com/)

### 私のUSBをさしたら、これが出る。

![street fighter 2 illustrator](https://farm2.staticflickr.com/1473/25065620132_9fc04a4145_o.png "street fighter 2 illustrator"){: .page_3-col_img}


---
layout : post
title : pair
category : products
date : 2014/10
thumbnail : https://farm2.staticflickr.com/1704/24490144404_b4a812e089_n.jpg
---

![pair](https://farm2.staticflickr.com/1463/25127844885_6046ef7431_b.jpg "pair"){: .page_head_img}

## Pairは二人の距離を縮めるグラス

pair二つが揃わないと自立しないグラスを制作しました。初めての相手とも、接する機会を積極的に与えていくグラスです。

写真は博多駅の屋上で撮影したものです。大勢のカップルを横目に撮影したのは、いい思い出です。
「グラス」というアイテムが、話すきっかけになるのは、ちょっと面白いかなと思っています。

## Tokyo Designers Week 2014に出展してきました。

Tokyo Designers Week 2014のテーマは「MY AVANT-GARDE」。学校展にて展示していました。学校全体で、企業賞を2ついただきました。
また大学内でのテーマは「日常への宣戦布告」。私のグループでは、男女の出会いの場での宣戦布告を考え、このPairというグラスを作りました。
5人グループで、このPairは制作しています。

## 制作

本当は、ガラス製を作りたかったんですが、自分で作ることもできず、また外注も厳しかったので、アクリルの真空成形で制作することにしました。
真空成形では、型が抜ける形でないと成形できないので、パーツをふたつに分けて、それをアクリル用の接着剤でくっつけました。接着跡は、アクリルサンダーで消えるんですが、底部分が削りづらく、制作は本当に難しいものでした。
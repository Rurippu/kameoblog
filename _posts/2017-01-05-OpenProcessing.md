---
layout : post
title : OpenProcessing
category : mediaArt
date : 2017/01/05
thumbnail : https://c1.staticflickr.com/1/409/32059941872_5d0c988039_m.jpg
---

![LifeGame](https://c1.staticflickr.com/1/409/32059941872_5d0c988039_m.jpg "LifeGame")

以前よく使っていたOpenProcessingが新しくなって、とっても綺麗なサイトになってました。

[OpenProcessing](https://www.openprocessing.org/ "OpenProcessing")


## OpenProcessingとは

processingという、簡単に描画をプログラムでかけるソフトがあって、それをweb上に公開できるサービスが、openProcessingです。
クラスの概念とかを教えてもらった時に、忘れまいとして、コードをopenProcessingに残していました。
花火を作るっていう課題は、クラスを学ぶ上で、とてもいい課題でした。

## Processing

プログラム学びたいけど、何言語がオススメ？という質問はいつも困ってしまうけど、そのたびに、processingを勧めてきた気がします。

初心者にとって、一番敷居が高くなるポイントは、開発環境を整えることなんじゃないかなと思っています。その点、processingはインストールするだけで環境は整うし、ビルドボタンもとってもわかりやすいし、UIが何より、シンプルだと思います。Android Studioとか押し間違えちゃいそう！
そして、言語はjavaがベースになってると思うんですが、関数名がすごくシンプルで、processingのリファレンスもすごく見やすいです。
何より、Arduinoの連携が簡単なのも素晴らしいポイント。

みんな、最終的にopenFrameworksに移るけど、なんでなんだろ。大きいことしようとすると、処理がoFの方が圧倒的に早いのかな。

## Life Game

Life Gameを浪人中の時に知って、まだガラケーだった頃に、携帯にLife Gameアプリみたいなのを入れて、いろんなパラメーターを試して遊んでいました。
Life Game自体、ずーっと見ていられるようなきれいなものなんだけど、もっときれいな見た目にしようと思って、徐々に消えるLifeGameを以前作っていました。

[LifeGame](https://www.openprocessing.org/sketch/60331 "LifeGame")
